
# GitLab Cloud Native Hybrid on EKS

## Easily Run Performance Tests on AWS Quick Start Prepared GitLab Instances

A set of performance testing instructions have been abbreviated for testing a GitLab instance prepared using the AWS Quick Start for GitLab Cloud Native Hybrid on EKS. They assume zero familiarity with GitLab Performance Tool. They can be access here: [Performance Testing an Instance Prepared using AWS Quick Start for GitLab Cloud Native Hybrid on EKS](Easy-DIY-Perf-Testing.md).

## Contribute Your Own Configuration w/ Performance Testing

Found a great way to tune up GitLab for EKS?  We'd love to have you contribute it to this repository. Create a fork and an MR back to this repository with a new file under [contributed-testing](contributed-testing) made from the template [test-results-contribution-template.md](contributed-testing/test-results-contribution-template.md)
