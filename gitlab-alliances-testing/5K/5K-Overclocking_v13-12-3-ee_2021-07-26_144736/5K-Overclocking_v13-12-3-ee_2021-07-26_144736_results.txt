* Environment:                5k-overclocking
* Environment Version:        13.12.3-ee `9d9769ba2ad`
* Option:                     thistestrps
* Date:                       2021-07-26
* Run Time:                   1h 21m 49.85s (Start: 14:47:36 UTC, End: 16:09:26 UTC)
* GPT Version:                v2.8.0

❯ Overall Results Score: 96.71%

NAME                                                     | RPS   | RPS RESULT           | TTFB AVG   | TTFB P90              | REQ STATUS     | RESULT          
---------------------------------------------------------|-------|----------------------|------------|-----------------------|----------------|-----------------
api_v4_groups                                            | 200/s | 174.2/s (>160.00/s)  | 1030.02ms  | 1214.69ms (<505ms)    | 100.00% (>99%) | FAILED²
api_v4_groups_group                                      | 200/s | 44.02/s (>16.00/s)   | 4167.74ms  | 5091.40ms (<7505ms)   | 100.00% (>99%) | Passed 
api_v4_groups_group_subgroups                            | 200/s | 196.73/s (>160.00/s) | 589.19ms   | 998.87ms (<505ms)     | 100.00% (>99%) | FAILED²
api_v4_groups_issues                                     | 200/s | 157.55/s (>48.00/s)  | 1161.56ms  | 2486.64ms (<3505ms)   | 100.00% (>99%) | Passed 
api_v4_groups_merge_requests                             | 200/s | 174.87/s (>48.00/s)  | 1039.25ms  | 2206.35ms (<3505ms)   | 100.00% (>99%) | Passed 
api_v4_groups_projects                                   | 200/s | 111.44/s (>80.00/s)  | 1623.62ms  | 3890.65ms (<3505ms)   | 100.00% (>99%) | FAILED²
api_v4_projects                                          | 200/s | 61.3/s (>24.00/s)    | 2754.62ms  | 6412.49ms (<7005ms)   | 100.00% (>99%) | Passed 
api_v4_projects_deploy_keys                              | 200/s | 199.17/s (>160.00/s) | 48.93ms    | 82.41ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_issues                                   | 200/s | 196.88/s (>160.00/s) | 267.01ms   | 915.46ms (<505ms)     | 100.00% (>99%) | FAILED²
api_v4_projects_issues_issue                             | 200/s | 196.45/s (>160.00/s) | 268.99ms   | 900.26ms (<1505ms)    | 100.00% (>99%) | Passed 
api_v4_projects_issues_search                            | 200/s | 69.41/s (>24.00/s)   | 2667.30ms  | 6029.68ms (<12005ms)  | 100.00% (>99%) | Passed 
api_v4_projects_languages                                | 200/s | 199.6/s (>160.00/s)  | 27.96ms    | 31.00ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests                           | 200/s | 197.52/s (>96.00/s)  | 141.09ms   | 179.34ms (<1005ms)    | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request             | 200/s | 196.4/s (>80.00/s)   | 182.47ms   | 247.62ms (<2755ms)    | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request_changes     | 200/s | 175.73/s (>80.00/s)  | 1008.30ms  | 1634.63ms (<3505ms)   | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request_commits     | 200/s | 199.14/s (>160.00/s) | 47.13ms    | 52.27ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request_discussions | 200/s | 197.98/s (>160.00/s) | 116.91ms   | 140.10ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_project                                  | 200/s | 198.19/s (>160.00/s) | 88.12ms    | 98.40ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_project_pipelines                        | 200/s | 199.42/s (>160.00/s) | 37.35ms    | 41.78ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_project_pipelines_pipeline               | 200/s | 198.82/s (>160.00/s) | 48.11ms    | 52.59ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_project_services                         | 200/s | 199.51/s (>160.00/s) | 29.91ms    | 36.12ms (<505ms)      | 91.49% (>99%)  | FAILED²
api_v4_projects_releases                                 | 200/s | 199.6/s (>160.00/s)  | 45.05ms    | 58.95ms (<505ms)      | 81.93% (>99%)  | FAILED²
api_v4_projects_repository_branches                      | 200/s | 199.47/s (>160.00/s) | 29.37ms    | 33.15ms (<505ms)      | 97.88% (>99%)  | FAILED²
api_v4_projects_repository_branches_branch               | 200/s | 198.84/s (>160.00/s) | 62.28ms    | 73.75ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_branches_search               | 200/s | 198.83/s (>48.00/s)  | 29.86ms    | 32.82ms (<6005ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits                       | 200/s | 199.26/s (>160.00/s) | 48.41ms    | 54.93ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits_commit                | 200/s | 198.58/s (>160.00/s) | 91.41ms    | 102.11ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits_commit_diff           | 200/s | 198.48/s (>160.00/s) | 101.84ms   | 119.41ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_compare                       | 200/s | 46.92/s (>16.00/s)   | 3934.37ms  | 4772.39ms (<8005ms)   | 100.00% (>99%) | Passed 
api_v4_projects_repository_files_file                    | 200/s | 198.61/s (>160.00/s) | 74.39ms    | 92.15ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_files_file_blame              | 200/s | 15.59/s (>1.60/s)    | 11319.37ms | 17318.19ms (<35005ms) | 100.00% (>99%) | Passed 
api_v4_projects_repository_files_file_raw                | 200/s | 198.94/s (>160.00/s) | 60.83ms    | 66.77ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_tags                          | 200/s | 131.8/s (>32.00/s)   | 1389.31ms  | 2594.18ms (<10005ms)  | 100.00% (>99%) | Passed 
api_v4_projects_repository_tree                          | 200/s | 198.97/s (>160.00/s) | 59.29ms    | 64.02ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_user                                              | 200/s | 199.44/s (>160.00/s) | 27.93ms    | 31.81ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_users                                             | 200/s | 199.06/s (>160.00/s) | 53.43ms    | 60.28ms (<505ms)      | 100.00% (>99%) | Passed 
git_ls_remote                                            | 20/s  | 19.97/s (>16.00/s)   | 37.70ms    | 40.25ms (<505ms)      | 100.00% (>99%) | Passed 
git_pull                                                 | 20/s  | 19.95/s (>16.00/s)   | 46.38ms    | 58.36ms (<505ms)      | 100.00% (>99%) | Passed 
git_push                                                 | 4/s   | 2.42/s (>3.20/s)     | 279.56ms   | 397.88ms (<1005ms)    | 100.00% (>99%) | Passed 
web_group                                                | 20/s  | 19.96/s (>16.00/s)   | 93.75ms    | 110.89ms (<505ms)     | 100.00% (>99%) | Passed 
web_group_issues                                         | 20/s  | 19.57/s (>16.00/s)   | 203.22ms   | 224.53ms (<505ms)     | 100.00% (>99%) | Passed 
web_group_merge_requests                                 | 20/s  | 19.64/s (>16.00/s)   | 197.81ms   | 218.36ms (<505ms)     | 100.00% (>99%) | Passed 
web_project                                              | 20/s  | 19.5/s (>16.00/s)    | 204.41ms   | 223.58ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_branches                                     | 20/s  | 18.93/s (>16.00/s)   | 639.84ms   | 982.68ms (<1005ms)    | 100.00% (>99%) | Passed 
web_project_branches_search                              | 20/s  | 18.97/s (>16.00/s)   | 631.32ms   | 891.56ms (<1305ms)    | 100.00% (>99%) | Passed 
web_project_commit                                       | 20/s  | 18.14/s (>3.20/s)    | 807.36ms   | 2166.02ms (<10005ms)  | 100.00% (>99%) | Passed 
web_project_commits                                      | 20/s  | 19.17/s (>16.00/s)   | 403.26ms   | 540.25ms (<755ms)     | 100.00% (>99%) | Passed 
web_project_file_blame                                   | 20/s  | 5.45/s (>0.16/s)     | 3227.18ms  | 4718.72ms (<7005ms)   | 100.00% (>99%) | Passed 
web_project_file_rendered                                | 20/s  | 19.22/s (>9.60/s)    | 587.90ms   | 1291.95ms (<3005ms)   | 100.00% (>99%) | Passed 
web_project_file_source                                  | 20/s  | 19.18/s (>1.60/s)    | 675.40ms   | 1393.57ms (<1705ms)   | 100.00% (>99%) | Passed 
web_project_files                                        | 20/s  | 19.36/s (>16.00/s)   | 134.32ms   | 164.01ms (<805ms)     | 100.00% (>99%) | Passed 
web_project_issue                                        | 20/s  | 19.74/s (>16.00/s)   | 221.68ms   | 654.74ms (<2005ms)    | 100.00% (>99%) | Passed 
web_project_issues                                       | 20/s  | 19.6/s (>16.00/s)    | 221.12ms   | 246.70ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_issues_search                                | 20/s  | 19.67/s (>16.00/s)   | 218.18ms   | 236.70ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_merge_request                                | 20/s  | 18.22/s (>6.40/s)    | 1107.09ms  | 3813.34ms (<7505ms)   | 100.00% (>99%) | Passed 
web_project_merge_request_changes                        | 20/s  | 19.56/s (>16.00/s)   | 317.51ms   | 566.44ms (<1505ms)    | 100.00% (>99%) | Passed 
web_project_merge_request_commits                        | 20/s  | 18.8/s (>9.60/s)     | 634.68ms   | 967.02ms (<1755ms)    | 100.00% (>99%) | Passed 
web_project_merge_requests                               | 20/s  | 19.56/s (>16.00/s)   | 233.34ms   | 263.00ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_pipelines                                    | 20/s  | 19.67/s (>9.60/s)    | 266.34ms   | 395.70ms (<1005ms)    | 100.00% (>99%) | Passed 
web_project_pipelines_pipeline                           | 20/s  | 19.65/s (>16.00/s)   | 430.80ms   | 934.95ms (<2505ms)    | 100.00% (>99%) | Passed 
web_project_repository_compare                           | 20/s  | 4.45/s (>0.80/s)     | 4047.96ms  | 4465.47ms (<7505ms)   | 100.00% (>99%) | Passed 
web_project_tags                                         | 20/s  | 18.87/s (>12.80/s)   | 546.00ms   | 618.55ms (<1505ms)    | 100.00% (>99%) | Passed 
web_user                                                 | 20/s  | 19.8/s (>9.60/s)     | 150.73ms   | 234.12ms (<4005ms)    | 100.00% (>99%) | Passed 

² Failure may not be clear from summary alone. Refer to the individual test's full output for further debugging.