* Environment:                3k-quickstart-arm-rds-cache
* Environment Version:        13.12.3-ee `9d9769ba2ad`
* Option:                     thistestrps
* Date:                       2021-07-23
* Run Time:                   1h 5m 51.81s (Start: 12:42:16 UTC, End: 13:48:08 UTC)
* GPT Version:                v2.8.0

❯ Overall Results Score: 99.39%

NAME                                                     | RPS  | RPS RESULT         | TTFB AVG   | TTFB P90              | REQ STATUS     | RESULT         
---------------------------------------------------------|------|--------------------|------------|-----------------------|----------------|----------------
api_v4_groups                                            | 60/s | 59.73/s (>48.00/s) | 68.75ms    | 76.09ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_groups_group                                      | 60/s | 57.86/s (>4.80/s)  | 653.23ms   | 932.66ms (<7505ms)    | 100.00% (>99%) | Passed
api_v4_groups_group_subgroups                            | 60/s | 59.71/s (>48.00/s) | 71.15ms    | 78.68ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_groups_issues                                     | 60/s | 59.22/s (>14.40/s) | 187.03ms   | 230.00ms (<3505ms)    | 100.00% (>99%) | Passed
api_v4_groups_merge_requests                             | 60/s | 59.14/s (>14.40/s) | 195.78ms   | 254.40ms (<3505ms)    | 100.00% (>99%) | Passed
api_v4_groups_projects                                   | 60/s | 58.3/s (>24.00/s)  | 659.58ms   | 1045.87ms (<3505ms)   | 100.00% (>99%) | Passed
api_v4_projects                                          | 60/s | 30.34/s (>7.20/s)  | 1802.11ms  | 2337.54ms (<7005ms)   | 100.00% (>99%) | Passed
api_v4_projects_deploy_keys                              | 60/s | 59.81/s (>48.00/s) | 40.02ms    | 44.40ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_issues                                   | 60/s | 59.27/s (>48.00/s) | 150.94ms   | 182.82ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_issues_issue                             | 60/s | 58.69/s (>48.00/s) | 174.71ms   | 223.65ms (<1505ms)    | 100.00% (>99%) | Passed
api_v4_projects_issues_search                            | 60/s | 42.35/s (>7.20/s)  | 1280.70ms  | 2485.61ms (<12005ms)  | 100.00% (>99%) | Passed
api_v4_projects_languages                                | 60/s | 59.88/s (>48.00/s) | 31.54ms    | 35.48ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_merge_requests                           | 60/s | 59.31/s (>28.80/s) | 139.41ms   | 162.87ms (<1005ms)    | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request             | 60/s | 59.03/s (>24.00/s) | 231.53ms   | 330.55ms (<2755ms)    | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request_changes     | 60/s | 49.62/s (>24.00/s) | 1082.15ms  | 1718.99ms (<3505ms)   | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request_commits     | 60/s | 59.73/s (>48.00/s) | 51.64ms    | 56.36ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request_discussions | 60/s | 59.45/s (>48.00/s) | 123.66ms   | 148.20ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_project                                  | 60/s | 59.61/s (>48.00/s) | 88.89ms    | 102.66ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_project_pipelines                        | 60/s | 59.82/s (>48.00/s) | 40.75ms    | 46.40ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_project_pipelines_pipeline               | 60/s | 59.65/s (>48.00/s) | 50.96ms    | 56.42ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_project_services                         | 60/s | 59.87/s (>48.00/s) | 35.18ms    | 40.46ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_releases                                 | 60/s | 59.61/s (>48.00/s) | 58.61ms    | 66.31ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_branches                      | 60/s | 59.53/s (>48.00/s) | 33.12ms    | 34.68ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_branches_branch               | 60/s | 59.71/s (>48.00/s) | 64.21ms    | 71.65ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_branches_search               | 60/s | 59.55/s (>14.40/s) | 31.05ms    | 34.75ms (<6005ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_commits                       | 60/s | 59.75/s (>48.00/s) | 52.85ms    | 57.79ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_commits_commit                | 60/s | 59.52/s (>48.00/s) | 114.84ms   | 147.02ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_commits_commit_diff           | 60/s | 59.43/s (>48.00/s) | 138.57ms   | 200.10ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_compare                       | 60/s | 12.03/s (>4.80/s)  | 4600.45ms  | 6272.13ms (<8005ms)   | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file                    | 60/s | 59.68/s (>48.00/s) | 71.50ms    | 74.53ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file_blame              | 60/s | 3.73/s (>0.48/s)   | 14172.85ms | 19254.37ms (<35005ms) | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file_raw                | 60/s | 59.67/s (>48.00/s) | 65.15ms    | 72.37ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_projects_repository_tags                          | 60/s | 34.41/s (>9.60/s)  | 1590.25ms  | 3117.94ms (<10005ms)  | 100.00% (>99%) | Passed
api_v4_projects_repository_tree                          | 60/s | 59.69/s (>48.00/s) | 68.94ms    | 76.58ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_user                                              | 60/s | 59.91/s (>48.00/s) | 31.07ms    | 36.46ms (<505ms)      | 100.00% (>99%) | Passed
api_v4_users                                             | 60/s | 59.81/s (>48.00/s) | 37.94ms    | 44.17ms (<505ms)      | 100.00% (>99%) | Passed
git_ls_remote                                            | 6/s  | 6.01/s (>4.80/s)   | 40.47ms    | 43.30ms (<505ms)      | 100.00% (>99%) | Passed
git_pull                                                 | 6/s  | 6.01/s (>4.80/s)   | 51.76ms    | 63.41ms (<505ms)      | 100.00% (>99%) | Passed
git_push                                                 | 2/s  | 1.86/s (>0.96/s)   | 261.42ms   | 419.27ms (<1005ms)    | 100.00% (>99%) | Passed
web_group                                                | 6/s  | 6.01/s (>4.80/s)   | 113.76ms   | 125.96ms (<505ms)     | 100.00% (>99%) | Passed
web_group_issues                                         | 6/s  | 5.92/s (>4.80/s)   | 209.39ms   | 228.30ms (<505ms)     | 100.00% (>99%) | Passed
web_group_merge_requests                                 | 6/s  | 5.96/s (>4.80/s)   | 201.26ms   | 227.77ms (<505ms)     | 100.00% (>99%) | Passed
web_project                                              | 6/s  | 5.95/s (>4.80/s)   | 203.79ms   | 230.43ms (<505ms)     | 100.00% (>99%) | Passed
web_project_branches                                     | 6/s  | 5.73/s (>4.80/s)   | 560.99ms   | 640.64ms (<1005ms)    | 100.00% (>99%) | Passed
web_project_branches_search                              | 6/s  | 5.71/s (>4.80/s)   | 619.41ms   | 685.74ms (<1305ms)    | 100.00% (>99%) | Passed
web_project_commit                                       | 6/s  | 5.63/s (>0.96/s)   | 600.73ms   | 1686.97ms (<10005ms)  | 100.00% (>99%) | Passed
web_project_commits                                      | 6/s  | 5.8/s (>4.80/s)    | 364.85ms   | 410.32ms (<755ms)     | 100.00% (>99%) | Passed
web_project_file_blame                                   | 6/s  | 1.73/s (>0.05/s)   | 2972.54ms  | 3655.15ms (<7005ms)   | 100.00% (>99%) | Passed
web_project_file_rendered                                | 6/s  | 5.14/s (>2.88/s)   | 631.40ms   | 663.16ms (<3005ms)    | 100.00% (>99%) | Passed
web_project_file_source                                  | 6/s  | 5.7/s (>0.48/s)    | 581.40ms   | 849.75ms (<1705ms)    | 100.00% (>99%) | Passed
web_project_files                                        | 6/s  | 5.89/s (>4.80/s)   | 149.17ms   | 174.75ms (<805ms)     | 100.00% (>99%) | Passed
web_project_issue                                        | 6/s  | 5.98/s (>4.80/s)   | 232.57ms   | 653.05ms (<2005ms)    | 100.00% (>99%) | Passed
web_project_issues                                       | 6/s  | 5.93/s (>4.80/s)   | 238.12ms   | 262.92ms (<505ms)     | 100.00% (>99%) | Passed
web_project_issues_search                                | 6/s  | 5.93/s (>4.80/s)   | 235.94ms   | 247.44ms (<505ms)     | 100.00% (>99%) | Passed
web_project_merge_request                                | 6/s  | 5.62/s (>1.92/s)   | 868.91ms   | 3636.71ms (<7505ms)   | 100.00% (>99%) | Passed
web_project_merge_request_changes                        | 6/s  | 5.82/s (>4.80/s)   | 335.33ms   | 580.27ms (<1505ms)    | 100.00% (>99%) | Passed
web_project_merge_request_commits                        | 6/s  | 5.69/s (>2.88/s)   | 578.56ms   | 614.00ms (<1755ms)    | 100.00% (>99%) | Passed
web_project_merge_requests                               | 6/s  | 5.94/s (>4.80/s)   | 234.42ms   | 286.22ms (<505ms)     | 100.00% (>99%) | Passed
web_project_pipelines                                    | 6/s  | 5.92/s (>2.88/s)   | 242.80ms   | 366.26ms (<1005ms)    | 100.00% (>99%) | Passed
web_project_pipelines_pipeline                           | 6/s  | 5.95/s (>4.80/s)   | 416.27ms   | 935.18ms (<2505ms)    | 100.00% (>99%) | Passed
web_project_repository_compare                           | 6/s  | 1.44/s (>0.24/s)   | 3872.29ms  | 4399.89ms (<7505ms)   | 100.00% (>99%) | Passed
web_project_tags                                         | 6/s  | 5.68/s (>3.84/s)   | 587.32ms   | 635.98ms (<1505ms)    | 100.00% (>99%) | Passed
web_user                                                 | 6/s  | 5.97/s (>2.88/s)   | 151.19ms   | 231.11ms (<4005ms)    | 100.00% (>99%) | Passed
