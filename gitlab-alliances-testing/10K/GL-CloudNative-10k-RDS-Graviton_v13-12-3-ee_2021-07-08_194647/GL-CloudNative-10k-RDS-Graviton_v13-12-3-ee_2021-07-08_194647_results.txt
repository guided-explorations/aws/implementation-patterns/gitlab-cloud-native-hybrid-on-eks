* Environment:                Gl-cloudnative-10k-rds-graviton
* Environment Version:        13.12.3-ee `9d9769ba2ad`
* Option:                     60s_200rps
* Date:                       2021-07-08
* Run Time:                   1h 6m 21.52s (Start: 19:46:47 UTC, End: 20:53:09 UTC)
* GPT Version:                v2.8.0

❯ Overall Results Score: 97.8%

NAME                                                     | RPS   | RPS RESULT           | TTFB AVG  | TTFB P90             | REQ STATUS     | RESULT         
---------------------------------------------------------|-------|----------------------|-----------|----------------------|----------------|----------------
api_v4_groups                                            | 200/s | 198.4/s (>160.00/s)  | 63.16ms   | 71.27ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_groups_group                                      | 200/s | 191.12/s (>16.00/s)  | 402.56ms  | 631.65ms (<7505ms)   | 100.00% (>99%) | Passed
api_v4_groups_group_subgroups                            | 200/s | 198.73/s (>160.00/s) | 68.55ms   | 78.02ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_groups_issues                                     | 200/s | 168.35/s (>48.00/s)  | 1043.17ms | 1426.39ms (<3505ms)  | 100.00% (>99%) | Passed
api_v4_groups_merge_requests                             | 200/s | 184.16/s (>48.00/s)  | 924.09ms  | 1274.29ms (<3505ms)  | 100.00% (>99%) | Passed
api_v4_groups_projects                                   | 200/s | 194.69/s (>80.00/s)  | 354.43ms  | 653.75ms (<3505ms)   | 100.00% (>99%) | Passed
api_v4_projects                                          | 200/s | 130.02/s (>24.00/s)  | 1382.47ms | 2351.01ms (<7005ms)  | 100.00% (>99%) | Passed
api_v4_projects_deploy_keys                              | 200/s | 199.16/s (>160.00/s) | 33.86ms   | 41.02ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_issues                                   | 200/s | 197.52/s (>160.00/s) | 137.49ms  | 170.00ms (<505ms)    | 100.00% (>99%) | Passed
api_v4_projects_issues_issue                             | 200/s | 195.77/s (>160.00/s) | 149.08ms  | 185.99ms (<1505ms)   | 100.00% (>99%) | Passed
api_v4_projects_issues_search                            | 200/s | 95.32/s (>24.00/s)   | 1940.84ms | 4214.80ms (<12005ms) | 100.00% (>99%) | Passed
api_v4_projects_languages                                | 200/s | 199.39/s (>160.00/s) | 27.76ms   | 32.57ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_merge_requests                           | 200/s | 197.69/s (>96.00/s)  | 139.11ms  | 186.07ms (<1005ms)   | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request             | 200/s | 195.48/s (>80.00/s)  | 175.35ms  | 219.69ms (<2755ms)   | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request_changes     | 200/s | 194.01/s (>80.00/s)  | 434.69ms  | 863.59ms (<3505ms)   | 99.94% (>99%)  | Passed
api_v4_projects_merge_requests_merge_request_commits     | 200/s | 198.19/s (>160.00/s) | 46.40ms   | 53.35ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_merge_requests_merge_request_discussions | 200/s | 197.45/s (>160.00/s) | 114.58ms  | 142.03ms (<505ms)    | 100.00% (>99%) | Passed
api_v4_projects_project                                  | 200/s | 198.44/s (>160.00/s) | 82.62ms   | 100.32ms (<505ms)    | 100.00% (>99%) | Passed
api_v4_projects_project_pipelines                        | 200/s | 199.29/s (>160.00/s) | 36.06ms   | 43.54ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_project_pipelines_pipeline               | 200/s | 198.56/s (>160.00/s) | 45.89ms   | 52.70ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_project_services                         | 200/s | 199.44/s (>160.00/s) | 31.64ms   | 37.81ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_releases                                 | 200/s | 198.75/s (>160.00/s) | 49.97ms   | 59.27ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_branches                      | 200/s | 198.7/s (>160.00/s)  | 28.97ms   | 32.96ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_branches_branch               | 200/s | 199.15/s (>160.00/s) | 56.23ms   | 66.18ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_branches_search               | 200/s | 198.99/s (>48.00/s)  | 28.37ms   | 33.01ms (<6005ms)    | 100.00% (>99%) | Passed
api_v4_projects_repository_commits                       | 200/s | 199.19/s (>160.00/s) | 44.84ms   | 52.58ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_commits_commit                | 200/s | 198.55/s (>160.00/s) | 81.71ms   | 91.28ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_commits_commit_diff           | 200/s | 196.91/s (>160.00/s) | 84.72ms   | 94.13ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_compare                       | 200/s | 92.62/s (>16.00/s)   | 1989.65ms | 2591.02ms (<8005ms)  | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file                    | 200/s | 197.41/s (>160.00/s) | 65.43ms   | 69.73ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file_blame              | 200/s | 30.91/s (>1.60/s)    | 5892.32ms | 8421.20ms (<35005ms) | 100.00% (>99%) | Passed
api_v4_projects_repository_files_file_raw                | 200/s | 199.0/s (>160.00/s)  | 58.01ms   | 64.59ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_projects_repository_tags                          | 200/s | 194.49/s (>32.00/s)  | 426.43ms  | 528.18ms (<10005ms)  | 100.00% (>99%) | Passed
api_v4_projects_repository_tree                          | 200/s | 199.08/s (>160.00/s) | 55.68ms   | 61.37ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_user                                              | 200/s | 199.42/s (>160.00/s) | 27.95ms   | 34.17ms (<505ms)     | 100.00% (>99%) | Passed
api_v4_users                                             | 200/s | 199.3/s (>160.00/s)  | 33.75ms   | 41.10ms (<505ms)     | 100.00% (>99%) | Passed
git_ls_remote                                            | 20/s  | 19.98/s (>16.00/s)   | 35.92ms   | 37.11ms (<505ms)     | 100.00% (>99%) | Passed
git_pull                                                 | 20/s  | 19.92/s (>16.00/s)   | 44.92ms   | 55.62ms (<505ms)     | 100.00% (>99%) | Passed
git_push                                                 | 4/s   | 2.6/s (>3.20/s)      | 276.71ms  | 402.76ms (<1005ms)   | 100.00% (>99%) | Passed
web_group                                                | 20/s  | 19.89/s (>16.00/s)   | 117.21ms  | 136.17ms (<505ms)    | 100.00% (>99%) | Passed
web_group_issues                                         | 20/s  | 19.45/s (>16.00/s)   | 209.49ms  | 247.72ms (<505ms)    | 100.00% (>99%) | Passed
web_group_merge_requests                                 | 20/s  | 19.65/s (>16.00/s)   | 197.18ms  | 237.94ms (<505ms)    | 100.00% (>99%) | Passed
web_project                                              | 20/s  | 19.62/s (>16.00/s)   | 206.90ms  | 249.75ms (<505ms)    | 100.00% (>99%) | Passed
web_project_branches                                     | 20/s  | 19.12/s (>16.00/s)   | 455.03ms  | 516.96ms (<1005ms)   | 100.00% (>99%) | Passed
web_project_branches_search                              | 20/s  | 18.98/s (>16.00/s)   | 516.49ms  | 568.41ms (<1305ms)   | 100.00% (>99%) | Passed
web_project_commit                                       | 20/s  | 18.72/s (>3.20/s)    | 567.05ms  | 1612.51ms (<10005ms) | 100.00% (>99%) | Passed
web_project_commits                                      | 20/s  | 19.08/s (>16.00/s)   | 367.06ms  | 414.31ms (<755ms)    | 100.00% (>99%) | Passed
web_project_file_blame                                   | 20/s  | 6.88/s (>0.16/s)     | 2512.66ms | 2646.86ms (<7005ms)  | 100.00% (>99%) | Passed
web_project_file_rendered                                | 20/s  | 16.8/s (>9.60/s)     | 623.45ms  | 664.76ms (<3005ms)   | 100.00% (>99%) | Passed
web_project_file_source                                  | 20/s  | 19.37/s (>1.60/s)    | 550.26ms  | 784.89ms (<1705ms)   | 100.00% (>99%) | Passed
web_project_files                                        | 20/s  | 19.46/s (>16.00/s)   | 130.16ms  | 161.80ms (<805ms)    | 100.00% (>99%) | Passed
web_project_issue                                        | 20/s  | 19.73/s (>16.00/s)   | 225.70ms  | 627.74ms (<2005ms)   | 100.00% (>99%) | Passed
web_project_issues                                       | 20/s  | 19.6/s (>16.00/s)    | 221.94ms  | 253.55ms (<505ms)    | 100.00% (>99%) | Passed
web_project_issues_search                                | 20/s  | 19.59/s (>16.00/s)   | 209.41ms  | 238.14ms (<505ms)    | 100.00% (>99%) | Passed
web_project_merge_request                                | 20/s  | 18.56/s (>6.40/s)    | 829.90ms  | 3430.20ms (<7505ms)  | 100.00% (>99%) | Passed
web_project_merge_request_changes                        | 20/s  | 19.44/s (>16.00/s)   | 314.20ms  | 543.95ms (<1505ms)   | 100.00% (>99%) | Passed
web_project_merge_request_commits                        | 20/s  | 18.76/s (>9.60/s)    | 574.86ms  | 793.63ms (<1755ms)   | 100.00% (>99%) | Passed
web_project_merge_requests                               | 20/s  | 19.62/s (>16.00/s)   | 218.49ms  | 260.09ms (<505ms)    | 100.00% (>99%) | Passed
web_project_pipelines                                    | 20/s  | 19.63/s (>9.60/s)    | 247.61ms  | 381.93ms (<1005ms)   | 100.00% (>99%) | Passed
web_project_pipelines_pipeline                           | 20/s  | 19.69/s (>16.00/s)   | 418.44ms  | 936.26ms (<2505ms)   | 100.00% (>99%) | Passed
web_project_repository_compare                           | 20/s  | 4.73/s (>0.80/s)     | 3808.79ms | 4296.01ms (<7505ms)  | 100.00% (>99%) | Passed
web_project_tags                                         | 20/s  | 19.02/s (>12.80/s)   | 514.64ms  | 576.85ms (<1505ms)   | 100.00% (>99%) | Passed
web_user                                                 | 20/s  | 19.85/s (>9.60/s)    | 148.58ms  | 230.68ms (<4005ms)   | 100.00% (>99%) | Passed
