* Environment:                50k-fixed-scale-test
* Environment Version:        13.12.3-ee `9d9769ba2ad`
* Option:                     thistestrps
* Date:                       2021-08-13
* Run Time:                   1h 13m 13.96s (Start: 17:28:19 UTC, End: 18:41:33 UTC)
* GPT Version:                v2.8.0

❯ Overall Results Score: 98.19%

NAME                                                     | RPS    | RPS RESULT           | TTFB AVG   | TTFB P90              | REQ STATUS     | RESULT          
---------------------------------------------------------|--------|----------------------|------------|-----------------------|----------------|-----------------
api_v4_groups                                            | 1000/s | 993.01/s (>800.00/s) | 70.78ms    | 84.92ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_groups_group                                      | 1000/s | 734.45/s (>80.00/s)  | 1161.86ms  | 1522.52ms (<7505ms)   | 100.00% (>99%) | Passed 
api_v4_groups_group_subgroups                            | 1000/s | 993.35/s (>800.00/s) | 74.38ms    | 87.24ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_groups_issues                                     | 1000/s | 555.47/s (>240.00/s) | 1650.11ms  | 2990.69ms (<3505ms)   | 99.99% (>99%)  | Passed 
api_v4_groups_merge_requests                             | 1000/s | 571.13/s (>240.00/s) | 1610.83ms  | 2849.82ms (<3505ms)   | 100.00% (>99%) | Passed 
api_v4_groups_projects                                   | 1000/s | 962.38/s (>400.00/s) | 725.30ms   | 1093.82ms (<3505ms)   | 99.99% (>99%)  | Passed 
api_v4_projects                                          | 1000/s | 486.42/s (>120.00/s) | 1859.65ms  | 2687.08ms (<7005ms)   | 100.00% (>99%) | Passed 
api_v4_projects_deploy_keys                              | 1000/s | 996.64/s (>800.00/s) | 35.81ms    | 42.67ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_issues                                   | 1000/s | 987.14/s (>800.00/s) | 173.09ms   | 273.59ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_issues_issue                             | 1000/s | 975.08/s (>800.00/s) | 188.97ms   | 310.86ms (<1505ms)    | 100.00% (>99%) | Passed 
api_v4_projects_issues_search                            | 1000/s | 55.05/s (>120.00/s)  | 16545.86ms | 21806.73ms (<12005ms) | 99.91% (>99%)  | FAILED²
api_v4_projects_languages                                | 1000/s | 996.2/s (>800.00/s)  | 29.31ms    | 33.81ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests                           | 1000/s | 988.05/s (>480.00/s) | 146.67ms   | 187.00ms (<1005ms)    | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request             | 1000/s | 975.95/s (>400.00/s) | 219.73ms   | 299.55ms (<2755ms)    | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request_changes     | 1000/s | 267.53/s (>400.00/s) | 444.59ms   | 611.29ms (<3505ms)    | 99.94% (>99%)  | FAILED²
api_v4_projects_merge_requests_merge_request_commits     | 1000/s | 995.07/s (>800.00/s) | 47.37ms    | 54.52ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request_discussions | 1000/s | 983.17/s (>800.00/s) | 124.50ms   | 156.68ms (<505ms)     | 99.99% (>99%)  | Passed 
api_v4_projects_project                                  | 1000/s | 991.42/s (>800.00/s) | 90.66ms    | 104.44ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_project_pipelines                        | 1000/s | 996.47/s (>800.00/s) | 38.35ms    | 43.68ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_project_pipelines_pipeline               | 1000/s | 987.16/s (>800.00/s) | 48.94ms    | 54.95ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_project_services                         | 1000/s | 996.48/s (>800.00/s) | 33.90ms    | 38.42ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_releases                                 | 1000/s | 991.64/s (>800.00/s) | 54.71ms    | 62.34ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_branches                      | 1000/s | 990.39/s (>800.00/s) | 30.69ms    | 33.79ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_branches_branch               | 1000/s | 989.28/s (>800.00/s) | 64.11ms    | 78.58ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_branches_search               | 1000/s | 993.93/s (>240.00/s) | 29.98ms    | 33.21ms (<6005ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits                       | 1000/s | 995.38/s (>800.00/s) | 48.93ms    | 56.05ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits_commit                | 1000/s | 991.98/s (>800.00/s) | 90.31ms    | 104.76ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits_commit_diff           | 1000/s | 991.36/s (>800.00/s) | 93.19ms    | 107.95ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_compare                       | 1000/s | 178.6/s (>80.00/s)   | 1990.40ms  | 3916.51ms (<8005ms)   | 99.94% (>99%)  | Passed 
api_v4_projects_repository_files_file                    | 1000/s | 983.85/s (>800.00/s) | 73.15ms    | 86.60ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_files_file_blame              | 1000/s | 60.1/s (>8.00/s)     | 2955.52ms  | 7037.15ms (<35005ms)  | 98.64% (>99%)  | FAILED²
api_v4_projects_repository_files_file_raw                | 1000/s | 989.21/s (>800.00/s) | 71.85ms    | 91.99ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_tags                          | 1000/s | 100.49/s (>160.00/s) | 8779.20ms  | 26707.44ms (<10005ms) | 93.54% (>99%)  | FAILED²
api_v4_projects_repository_tree                          | 1000/s | 993.74/s (>800.00/s) | 73.61ms    | 84.06ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_user                                              | 1000/s | 996.44/s (>800.00/s) | 29.48ms    | 33.27ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_users                                             | 1000/s | 996.41/s (>800.00/s) | 35.27ms    | 40.20ms (<505ms)      | 100.00% (>99%) | Passed 
git_ls_remote                                            | 100/s  | 99.64/s (>80.00/s)   | 36.63ms    | 38.94ms (<505ms)      | 100.00% (>99%) | Passed 
git_pull                                                 | 100/s  | 99.3/s (>80.00/s)    | 45.87ms    | 57.92ms (<505ms)      | 100.00% (>99%) | Passed 
git_push                                                 | 20/s   | 18.18/s (>16.00/s)   | 327.08ms   | 388.25ms (<1005ms)    | 100.00% (>99%) | Passed 
web_group                                                | 100/s  | 99.16/s (>80.00/s)   | 110.23ms   | 126.43ms (<505ms)     | 100.00% (>99%) | Passed 
web_group_issues                                         | 100/s  | 97.18/s (>80.00/s)   | 214.08ms   | 249.87ms (<505ms)     | 100.00% (>99%) | Passed 
web_group_merge_requests                                 | 100/s  | 97.74/s (>80.00/s)   | 212.33ms   | 244.53ms (<505ms)     | 100.00% (>99%) | Passed 
web_project                                              | 100/s  | 97.82/s (>80.00/s)   | 218.59ms   | 249.21ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_branches                                     | 100/s  | 94.49/s (>80.00/s)   | 521.39ms   | 581.17ms (<1005ms)    | 100.00% (>99%) | Passed 
web_project_branches_search                              | 100/s  | 94.42/s (>80.00/s)   | 585.92ms   | 638.00ms (<1305ms)    | 100.00% (>99%) | Passed 
web_project_commit                                       | 100/s  | 92.53/s (>16.00/s)   | 579.24ms   | 1629.75ms (<10005ms)  | 100.00% (>99%) | Passed 
web_project_commits                                      | 100/s  | 94.94/s (>80.00/s)   | 383.00ms   | 428.70ms (<755ms)     | 100.00% (>99%) | Passed 
web_project_file_blame                                   | 100/s  | 16.17/s (>0.80/s)    | 2557.68ms  | 2695.41ms (<7005ms)   | 100.00% (>99%) | Passed 
web_project_file_rendered                                | 100/s  | 83.27/s (>48.00/s)   | 642.05ms   | 672.59ms (<3005ms)    | 100.00% (>99%) | Passed 
web_project_file_source                                  | 100/s  | 24.02/s (>8.00/s)    | 568.35ms   | 811.96ms (<1705ms)    | 100.00% (>99%) | Passed 
web_project_files                                        | 100/s  | 97.42/s (>80.00/s)   | 140.99ms   | 174.83ms (<805ms)     | 100.00% (>99%) | Passed 
web_project_issue                                        | 100/s  | 97.97/s (>80.00/s)   | 229.09ms   | 647.18ms (<2005ms)    | 100.00% (>99%) | Passed 
web_project_issues                                       | 100/s  | 97.59/s (>80.00/s)   | 226.06ms   | 256.80ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_issues_search                                | 100/s  | 97.83/s (>80.00/s)   | 222.25ms   | 247.74ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_merge_request                                | 100/s  | 92.28/s (>32.00/s)   | 846.98ms   | 3548.30ms (<7505ms)   | 100.00% (>99%) | Passed 
web_project_merge_request_changes                        | 100/s  | 95.74/s (>80.00/s)   | 328.14ms   | 565.29ms (<1505ms)    | 100.00% (>99%) | Passed 
web_project_merge_request_commits                        | 100/s  | 93.28/s (>48.00/s)   | 577.83ms   | 634.10ms (<1755ms)    | 100.00% (>99%) | Passed 
web_project_merge_requests                               | 100/s  | 97.42/s (>80.00/s)   | 239.75ms   | 277.55ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_pipelines                                    | 100/s  | 98.33/s (>48.00/s)   | 248.68ms   | 370.39ms (<1005ms)    | 100.00% (>99%) | Passed 
web_project_pipelines_pipeline                           | 100/s  | 98.01/s (>80.00/s)   | 418.26ms   | 923.33ms (<2505ms)    | 100.00% (>99%) | Passed 
web_project_repository_compare                           | 100/s  | 22.59/s (>4.00/s)    | 3956.49ms  | 4452.50ms (<7505ms)   | 100.00% (>99%) | Passed 
web_project_tags                                         | 100/s  | 94.37/s (>64.00/s)   | 572.78ms   | 632.72ms (<1505ms)    | 100.00% (>99%) | Passed 
web_user                                                 | 100/s  | 99.09/s (>48.00/s)   | 150.67ms   | 230.24ms (<4005ms)    | 100.00% (>99%) | Passed 

² Failure may not be clear from summary alone. Refer to the individual test's full output for further debugging.