* Environment:                50k-autoscale-test
* Environment Version:        13.12.3-ee `9d9769ba2ad`
* Option:                     thistestrps
* Date:                       2021-08-13
* Run Time:                   1h 34m 17.93s (Start: 19:26:33 UTC, End: 21:00:51 UTC)
* GPT Version:                v2.8.0

❯ Overall Results Score: 96.04%

NAME                                                     | RPS    | RPS RESULT           | TTFB AVG   | TTFB P90              | REQ STATUS     | RESULT          
---------------------------------------------------------|--------|----------------------|------------|-----------------------|----------------|-----------------
api_v4_groups                                            | 1000/s | 961.25/s (>800.00/s) | 799.81ms   | 1196.68ms (<505ms)    | 100.00% (>99%) | FAILED²
api_v4_groups_group                                      | 1000/s | 233.78/s (>80.00/s)  | 3878.55ms  | 4975.71ms (<7505ms)   | 100.00% (>99%) | Passed 
api_v4_groups_group_subgroups                            | 1000/s | 980.36/s (>800.00/s) | 648.49ms   | 979.58ms (<505ms)     | 100.00% (>99%) | FAILED²
api_v4_groups_issues                                     | 1000/s | 557.17/s (>240.00/s) | 1649.14ms  | 2522.73ms (<3505ms)   | 99.99% (>99%)  | Passed 
api_v4_groups_merge_requests                             | 1000/s | 577.03/s (>240.00/s) | 1598.57ms  | 2478.61ms (<3505ms)   | 100.00% (>99%) | Passed 
api_v4_groups_projects                                   | 1000/s | 358.37/s (>400.00/s) | 2492.32ms  | 4260.89ms (<3505ms)   | 100.00% (>99%) | FAILED²
api_v4_projects                                          | 1000/s | 165.8/s (>120.00/s)  | 5085.62ms  | 8760.75ms (<7005ms)   | 100.00% (>99%) | FAILED²
api_v4_projects_deploy_keys                              | 1000/s | 995.26/s (>800.00/s) | 63.93ms    | 109.81ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_issues                                   | 1000/s | 731.21/s (>800.00/s) | 1252.51ms  | 2268.54ms (<505ms)    | 100.00% (>99%) | FAILED²
api_v4_projects_issues_issue                             | 1000/s | 678.47/s (>800.00/s) | 1344.14ms  | 2472.81ms (<1505ms)   | 100.00% (>99%) | FAILED²
api_v4_projects_issues_search                            | 1000/s | 71.28/s (>120.00/s)  | 12429.63ms | 20883.98ms (<12005ms) | 100.00% (>99%) | FAILED²
api_v4_projects_languages                                | 1000/s | 995.74/s (>800.00/s) | 44.27ms    | 81.27ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests                           | 1000/s | 763.64/s (>480.00/s) | 1200.50ms  | 2148.67ms (<1005ms)   | 100.00% (>99%) | FAILED²
api_v4_projects_merge_requests_merge_request             | 1000/s | 602.09/s (>400.00/s) | 1510.47ms  | 2665.20ms (<2755ms)   | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request_changes     | 1000/s | 302.05/s (>400.00/s) | 2913.44ms  | 5016.94ms (<3505ms)   | 99.97% (>99%)  | FAILED²
api_v4_projects_merge_requests_merge_request_commits     | 1000/s | 990.77/s (>800.00/s) | 128.90ms   | 294.70ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_merge_requests_merge_request_discussions | 1000/s | 878.9/s (>800.00/s)  | 1030.16ms  | 1874.72ms (<505ms)    | 99.99% (>99%)  | FAILED²
api_v4_projects_project                                  | 1000/s | 987.3/s (>800.00/s)  | 224.00ms   | 607.63ms (<505ms)     | 100.00% (>99%) | FAILED²
api_v4_projects_project_pipelines                        | 1000/s | 995.42/s (>800.00/s) | 75.63ms    | 196.54ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_project_pipelines_pipeline               | 1000/s | 991.57/s (>800.00/s) | 94.75ms    | 236.19ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_project_services                         | 1000/s | 996.29/s (>800.00/s) | 50.38ms    | 110.70ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_releases                                 | 1000/s | 991.93/s (>800.00/s) | 85.65ms    | 156.81ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_branches                      | 1000/s | 991.27/s (>800.00/s) | 38.78ms    | 62.32ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_branches_branch               | 1000/s | 994.31/s (>800.00/s) | 82.58ms    | 125.28ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_branches_search               | 1000/s | 989.48/s (>240.00/s) | 34.57ms    | 44.29ms (<6005ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits                       | 1000/s | 994.92/s (>800.00/s) | 59.35ms    | 86.79ms (<505ms)      | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits_commit                | 1000/s | 991.75/s (>800.00/s) | 98.18ms    | 122.56ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_commits_commit_diff           | 1000/s | 991.01/s (>800.00/s) | 107.50ms   | 134.12ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_compare                       | 1000/s | 176.28/s (>80.00/s)  | 2286.98ms  | 3959.86ms (<8005ms)   | 99.95% (>99%)  | Passed 
api_v4_projects_repository_files_file                    | 1000/s | 979.25/s (>800.00/s) | 127.20ms   | 188.56ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_files_file_blame              | 1000/s | 58.65/s (>8.00/s)    | 3204.23ms  | 6239.11ms (<35005ms)  | 98.66% (>99%)  | FAILED²
api_v4_projects_repository_files_file_raw                | 1000/s | 992.42/s (>800.00/s) | 98.53ms    | 165.44ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_projects_repository_tags                          | 1000/s | 109.71/s (>160.00/s) | 8021.72ms  | 17580.49ms (<10005ms) | 100.00% (>99%) | FAILED²
api_v4_projects_repository_tree                          | 1000/s | 993.56/s (>800.00/s) | 80.47ms    | 117.47ms (<505ms)     | 100.00% (>99%) | Passed 
api_v4_user                                              | 1000/s | 997.38/s (>800.00/s) | 29.08ms    | 34.37ms (<505ms)      | 95.18% (>99%)  | FAILED²
api_v4_users                                             | 1000/s | 997.42/s (>800.00/s) | 37.44ms    | 45.45ms (<505ms)      | 98.29% (>99%)  | FAILED²
git_ls_remote                                            | 100/s  | 99.66/s (>80.00/s)   | 37.03ms    | 39.80ms (<505ms)      | 100.00% (>99%) | Passed 
git_pull                                                 | 100/s  | 99.36/s (>80.00/s)   | 44.50ms    | 56.00ms (<505ms)      | 99.61% (>99%)  | Passed 
git_push                                                 | 20/s   | 18.19/s (>16.00/s)   | 329.84ms   | 391.68ms (<1005ms)    | 100.00% (>99%) | Passed 
web_group                                                | 100/s  | 99.23/s (>80.00/s)   | 102.00ms   | 121.78ms (<505ms)     | 100.00% (>99%) | Passed 
web_group_issues                                         | 100/s  | 97.41/s (>80.00/s)   | 208.97ms   | 234.19ms (<505ms)     | 100.00% (>99%) | Passed 
web_group_merge_requests                                 | 100/s  | 97.67/s (>80.00/s)   | 210.60ms   | 234.64ms (<505ms)     | 100.00% (>99%) | Passed 
web_project                                              | 100/s  | 97.94/s (>80.00/s)   | 206.75ms   | 232.24ms (<505ms)     | 99.20% (>99%)  | Passed 
web_project_branches                                     | 100/s  | 94.48/s (>80.00/s)   | 761.46ms   | 1163.31ms (<1005ms)   | 100.00% (>99%) | FAILED²
web_project_branches_search                              | 100/s  | 88.36/s (>80.00/s)   | 984.83ms   | 1720.49ms (<1305ms)   | 100.00% (>99%) | FAILED²
web_project_commit                                       | 100/s  | 67.52/s (>16.00/s)   | 1494.56ms  | 4452.76ms (<10005ms)  | 100.00% (>99%) | Passed 
web_project_commits                                      | 100/s  | 94.9/s (>80.00/s)    | 707.18ms   | 1458.13ms (<755ms)    | 100.00% (>99%) | FAILED²
web_project_file_blame                                   | 100/s  | 16.05/s (>0.80/s)    | 3890.77ms  | 6837.14ms (<7005ms)   | 100.00% (>99%) | Passed 
web_project_file_rendered                                | 100/s  | 91.63/s (>48.00/s)   | 1228.55ms  | 2606.73ms (<3005ms)   | 100.00% (>99%) | Passed 
web_project_file_source                                  | 100/s  | 24.28/s (>8.00/s)    | 616.67ms   | 1074.19ms (<1705ms)   | 100.00% (>99%) | Passed 
web_project_files                                        | 100/s  | 97.74/s (>80.00/s)   | 134.93ms   | 168.01ms (<805ms)     | 100.00% (>99%) | Passed 
web_project_issue                                        | 100/s  | 98.54/s (>80.00/s)   | 236.50ms   | 670.84ms (<2005ms)    | 100.00% (>99%) | Passed 
web_project_issues                                       | 100/s  | 97.62/s (>80.00/s)   | 244.46ms   | 302.93ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_issues_search                                | 100/s  | 97.79/s (>80.00/s)   | 234.30ms   | 271.72ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_merge_request                                | 100/s  | 47.56/s (>32.00/s)   | 3339.66ms  | 8454.28ms (<7505ms)   | 100.00% (>99%) | FAILED²
web_project_merge_request_changes                        | 100/s  | 94.13/s (>80.00/s)   | 472.85ms   | 910.25ms (<1505ms)    | 100.00% (>99%) | Passed 
web_project_merge_request_commits                        | 100/s  | 79.5/s (>48.00/s)    | 1123.09ms  | 2377.54ms (<1755ms)   | 100.00% (>99%) | FAILED²
web_project_merge_requests                               | 100/s  | 97.22/s (>80.00/s)   | 256.68ms   | 324.80ms (<505ms)     | 100.00% (>99%) | Passed 
web_project_pipelines                                    | 100/s  | 98.03/s (>48.00/s)   | 281.72ms   | 427.73ms (<1005ms)    | 100.00% (>99%) | Passed 
web_project_pipelines_pipeline                           | 100/s  | 96.58/s (>80.00/s)   | 643.66ms   | 1491.83ms (<2505ms)   | 100.00% (>99%) | FAILED²
web_project_repository_compare                           | 100/s  | 12.71/s (>4.00/s)    | 6771.62ms  | 12976.86ms (<7505ms)  | 100.00% (>99%) | FAILED²
web_project_tags                                         | 100/s  | 93.12/s (>64.00/s)   | 889.88ms   | 1389.24ms (<1505ms)   | 100.00% (>99%) | Passed 
web_user                                                 | 100/s  | 99.02/s (>48.00/s)   | 149.23ms   | 236.82ms (<4005ms)    | 100.00% (>99%) | Passed 

² Failure may not be clear from summary alone. Refer to the individual test's full output for further debugging.
