# Easy Performance Testing an Instance Prepared using AWS Quick Start for GitLab Cloud Native Hybrid on EKS

This is now housed here: [Easy Performance Testing for AWS Quick Start for GitLab](https://gitlab.com/guided-explorations/aws/implementation-patterns/getting-started-gitlab-aws-quick-start/-/wikis/Easy-Performance-Testing-for-AWS-Quick-Start-for-GitLab-CNH)
